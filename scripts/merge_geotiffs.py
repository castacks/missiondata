#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys

gdal.UseExceptions()
if len(sys.argv) < 4:
    print 'Usage: merge_geotiffs.py <output geotiff> <extent geotiff> [secondary geotiffs...]'
    print ''
    print 'Generates a geotiff the size of extent geotiff with the sum of all pixel values.'
    print 'Undefined pixels are treated as 0.0'
    exit(1)

def getPoint(b,s,a,x,y):
    '''b: secondary geotiff bounding box
       s: secondary geotiff
       a: secondary geotiff's array
       x: x coord
       y: y coord'''
    (xmin,ymin,xmax,ymax) = b
    if not(x >= xmin and x< xmax and y>= ymin and y<ymax):
        return 0.0
    t = s.GetGeoTransform()
    xi = round((x - t[0])/t[1])
    yi = round((y - t[3])/t[5])
    if xi < 0 or xi >= len(a[0]) or yi < 0 or yi >= len(a):
        return 0.0
    return a[yi,xi]    
        

extent = gdal.Open(sys.argv[2],GA_ReadOnly)
if extent is None:
    print 'Error: Extent geotiff at '+sys.argv[2]+' not found/unable to be opened.'    
    exit(1)
secondaries = []
for fname in sys.argv[3:]:
    secondaries.append(gdal.Open(fname,GA_ReadOnly))
    if secondaries[-1] is None:
        print 'Error: Secondary geotiff at '+fname+' not found/unable to be opened.'    
        exit(1)

out_format = 'GTiff'
driver = gdal.GetDriverByName( out_format )

out = driver.CreateCopy(sys.argv[1], extent, 0)
if out is None:
    print 'Error: Failed to create output file at '+sys.argv[1]
    exit(1)

points = out.GetRasterBand(1).ReadAsArray()

boxes = []
arrays = []
for s in secondaries:
    t = s.GetGeoTransform()
    xmin = t[0]
    xmax = t[0] + t[1] * s.RasterXSize
    ymax = t[3]
    ymin = t[3] + t[5] * s.RasterYSize
    boxes.append((xmin,ymin,xmax,ymax))
    arrays.append(s.GetRasterBand(1).ReadAsArray())

t = extent.GetGeoTransform()
for yi in range(len(points)):
    if yi%50 == 0:
        print 'At row ' + str(yi) + ' of ' +str(len(points))
    for xi in range(len(points[0])):
        x = t[0] + t[1] * xi
        y = t[3] + t[5] * yi
        for b,s,a in zip(boxes,secondaries,arrays):
            points[yi,xi] = points[yi,xi]+getPoint(b,s,a,x,y)

out.GetRasterBand(1).WriteArray(points)

# Once we're done, close properly the datasets
extent = None
for i in range(len(secondaries)):
    secondaries[i] = None
out = None
