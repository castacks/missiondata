#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random
import math


#===============================================================================
#Gloval variables

tree_max_height = 15
tree_min_height = 5

output_data = None 
xsize = 0
ysize = 0


#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
MAX_VALID_POINT = 699.0


if len(sys.argv) != 4:
    print 'Usage: sim_city_dem_add_buildings.py <input DEM GeoTiff> <structures location text> <output DEM GeoTiff>'
    print ''
    print 'Generates a DEM GeoTiff with a list of buildings inserted at specified locations (text input).'
    print ''
    exit(1)


#===============================================================================
#Load input raster file image (DEM)

dataset = gdal.Open( sys.argv[1], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
    
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'


band = dataset.GetRasterBand(1)

print 'Band Type=',gdal.GetDataTypeName(band.DataType)

min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

scandata = band.ReadRaster( 0, 0, xsize, ysize, \
                            xsize, ysize, band.DataType )
#print len(scandata)

tuple_of_bytes = struct.unpack(data_types[gdal.GetDataTypeName(band.DataType)] * band.XSize * band.YSize, scandata)

#Print first element
#print tuple_of_bytes[0]
#Print last element
#print tuple_of_bytes[band.XSize * band.YSize - 1]

output_data = array.array('f', tuple_of_bytes)
index = 0
while (index < xsize*ysize):
    output_data[index] = 0;
    index = index + 1


#===============================================================================
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)


#===============================================================================
#Add structures with proper size
#B;EAST;NORTH;SIZEX;SIZEY;HEIGHT  >>> buildings
#T;EAST;NORTH;SIZEX;SIZEY;HEIGHT  >>> trees

pixel_size = geotransform[1]
if (pixel_size < 0):
    pixel_size = -1 * pixel_size

with open(sys.argv[2]) as f:
    for l in f:
        tmp = l.strip().split(';')
        print tmp
        st = tmp[0]
        E = float(tmp[1])
        N = float(tmp[2])
        sizeX = float(tmp[3])
        sizeY = float(tmp[4])
        height = float(tmp[5])

        xpos = int(round((E - minE) / abs(geotransform[1])))
        ypos = int(round((maxN - N) / abs(geotransform[5])))
        width  = int((sizeX/pixel_size) + 1)
        length = int((sizeY/pixel_size) + 1)

        #offsetX =  int(width /2.0)
        #offsetY = -int(length /2.0)
        
        for dx in range(0, width):
            for dy in range(0, length):
                if (st == 'B'): # building
                    output_data[ (xpos + dx) + (ypos + dy) * xsize ] = height + 5
                else: #'T' trees - random height
                    output_data[ (xpos + dx) + (ypos + dy) * xsize ] = int(height * random.random() + 1)

#===============================================================================
#Save output raster image file (DEM)

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[3], xsize, ysize, dataset.RasterCount , band.DataType )

dst_ds.SetGeoTransform( dataset.GetGeoTransform() )
dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount


#Set altitude offset - need to consider tower size
altitude_offset = 0


for y in range(0, ysize):
    print y
    aux_str = ''
    for x in range(0, xsize):
        aux_value = tuple_of_bytes[x+ y*xsize] - altitude_offset
        aux_value = aux_value + output_data[x + y*xsize]
        if (aux_value < 0.0):
            aux_value = 0.0 
        aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band.DataType)], aux_value)

    dst_ds.GetRasterBand(1).WriteRaster(0, y, xsize, 1, aux_str)


# Once we're done, close properly the datasets
dataset = None
dst_ds = None



