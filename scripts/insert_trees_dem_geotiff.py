#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random


#===============================================================================
#Gloval variables

tree_max_height = 8
tree_min_height = 4

output_data = None 
xsize = 0
ysize = 0


#===============================================================================

#add random height at defined position 
def add_random_height(posx, posy):
    if ((posx >= 0) and (posx < xsize) and (posy >= 0) and (posy < ysize)):
        output_data[ posx + posy * xsize ] = random.randint(tree_min_height, tree_max_height);
        #output_data[ posx + posy * xsize ] = 255;
        print '*'


#function to insert trees on the DEM
def add_trees(posx, posy):
    radius = 2
    i = -radius
    while (i < radius):
        j = -radius
        while (j < radius):
            if ((i*i + j*j) <= (radius * radius)):
                add_random_height(posx + i, posy + j)
            j = j + 1
        i = i + 1
    '''
    radius = 8
    i = -radius
    while (i < radius):
        j = -radius
        while (j < radius):
            if ((i*i + j*j) <= (radius * radius)):
                add_random_height(posx + i, posy + j)
            j = j + 1
        i = i + 1
    '''

#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
MAX_VALID_POINT = 699.0


if len(sys.argv) != 4:
    print 'Usage: insert_trees_dem_geotiff.py <input DEM GeoTiff> <input text file with trees locations> <output DEM GeoTiff>'
    print ''
    print 'Generates a DEM GeoTiff with trees inserted at specified locations (input text file).'
    print ''
    exit(1)


#===============================================================================
#Load input raster file image (DEM)

dataset = gdal.Open( sys.argv[1], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
    
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'


band = dataset.GetRasterBand(1)

print 'Band Type=',gdal.GetDataTypeName(band.DataType)

min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

scandata = band.ReadRaster( 0, 0, xsize, ysize, \
                            xsize, ysize, band.DataType )
#print len(scandata)

tuple_of_bytes = struct.unpack(data_types[gdal.GetDataTypeName(band.DataType)] * band.XSize * band.YSize, scandata)

#Print first element
#print tuple_of_bytes[0]
#Print last element
#print tuple_of_bytes[band.XSize * band.YSize - 1]

output_data = array.array('f', tuple_of_bytes)
index = 0
while (index < xsize*ysize):
    output_data[index] = 0;
    index = index + 1

#===============================================================================
#Load tree locations input text file
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)

E = 0
N = 0
with open(sys.argv[2]) as f:
    for l in f:
        tmp = l.strip().split(';')
        E = float(tmp[0])
        N = float(tmp[1])
        #print E, N
        if ((E > minE) and (E < maxE) and (N > minN) and (N < maxN)):
            xpos = int(round((E - minE) / abs(geotransform[1])))
            ypos = int(round((maxN - N) / abs(geotransform[5])))
            #print xpos, ypos
            add_trees(xpos, ypos)
            #output_data[ ypos * xsize + xpos ] = 255;



#===============================================================================
#Save output raster image file (DEM)

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[3], xsize, ysize, dataset.RasterCount , band.DataType )

dst_ds.SetGeoTransform( dataset.GetGeoTransform() )
dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount

altitude_offset = 2 * tree_max_height

aux_str = ''
index = 0
while (index < xsize*ysize):
    aux_value = tuple_of_bytes[index] - altitude_offset
    aux_value = aux_value + output_data[index]
    if (aux_value < 0):
        aux_value = 0 
    if (aux_value > 255):
        aux_value = 255
    aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band.DataType)], aux_value)
    index = index + 1 

dst_ds.GetRasterBand(1).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(2).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(3).WriteRaster(0, 0, xsize, ysize, aux_str)



# Once we're done, close properly the datasets
dataset = None
dst_ds = None



