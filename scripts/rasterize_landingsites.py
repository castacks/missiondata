#!/usr/bin/env python
'''Convert a shapefile into a bizarre rasterized geotiff for honeywell'''
import sys
import osgeo.ogr
import numpy as np

map_offset = (500000, 4500000) # easting, northing
map_min = (5+map_offset[0],5+map_offset[1]) # set up so each point will be the center of its region
map_max = (20476+map_offset[0],20476+map_offset[1])
map_res = 10

if len(sys.argv) != 3:
    print 'Usage: rasterize_landingsites.py <input shape file> <output geotiff>'
    print ''
    print 'Generates a landingsites geotiff for honeywell from a shapefile'
    exit(1)

ds = osgeo.ogr.Open(sys.argv[1])
if ds is None:
    print "Open failed on shapefile.\n"
    sys.exit( 1 )

lyr = ds.GetLayerByName( "landingsites" )

lyr.ResetReading()

x_coords = range(map_min[0],map_max[0],map_res)
dim_x = len(x_coords)
y_coords = range(map_min[1],map_max[1],map_res)
dim_y = len(y_coords)
rast = np.zeros((dim_x,dim_y),dtype='int')
testpoint = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)


for feat in lyr:
    print 'New feature:'
    field_type = None
    feat_defn = lyr.GetLayerDefn()
    field_type = feat.GetFieldAsString(0)            
    geom = feat.GetGeometryRef()

    if field_type == 'No Fly Zone':
        print 'No fly zone found'
        field_sym = 1             
    elif field_type == 'Potential Landing Site':
        print 'Landing zone found'
        field_sym = 2
    else:
        continue

    for (ii,x) in zip(range(len(x_coords)),x_coords):
        if ii%100 == 0:
            print 'At row %d' % (ii,)
        for (jj,y) in zip(range(len(y_coords)),y_coords):
            testpoint.SetPoint(0,x,y)
            if geom.Contains(testpoint):
                rast[ii,jj] |= field_sym
ds = None
f = open(sys.argv[2],'w')
for jj in range(len(y_coords)-1,-1,-1):
    f.write(str(rast[0,jj]))
    for ii in range(1,len(x_coords)):
        # remember, x is easting, y is northing
        f.write(','+str(rast[ii,jj]))
    f.write('\n')
f.close()
