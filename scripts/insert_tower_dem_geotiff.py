#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random


#===============================================================================
#Gloval variables

#tree_max_height = 15
#tree_min_height = 5

output_data = None 
xsize = 0
ysize = 0


#===============================================================================


#function to insert tower in the DEM
#height / base_width / top_width in meters 60 * 7.5 * 1.5
def add_tower(posx, posy, height):
    increment = height / 3.0

    i = 0
    while (i < 3):
        j = 0
        while (j < 3):
            output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + increment
            j = j + 1
        i = i + 1

    i = 1
    j = 1
    output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + 2*increment

    '''
    i = 0
    while (i < 5):
        j = 0
        while (j < 5):
            output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + increment
            j = j + 1
        i = i + 1
    
    i = 1
    while (i < 4):
        j = 1
        while (j < 4):
            output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + increment
            j = j + 1
        i = i + 1

    i = 2
    j = 2
    output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + increment
    '''

    '''
    increment = height / 8.0

    i = 0
    while (i < 9):
        j = 0
        while (j < 9):
            output_data[ (posx + i) + (posy + j) * xsize ] = increment
            j = j + 1
        i = i + 1

    i = 1
    while (i < 8):
        j = 1
        while (j < 8):
            output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + increment
            j = j + 1
        i = i + 1

    i = 2
    while (i < 7):
        j = 2
        while (j < 7):
            output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + increment
            j = j + 1
        i = i + 1

    i = 3
    while (i < 6):
        j = 3
        while (j < 6):
            output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + 2.0*increment
            j = j + 1
        i = i + 1

    i = 4
    j = 4
    output_data[ (posx + i) + (posy + j) * xsize ] = output_data[ (posx + i) + (posy + j) * xsize ] + 3.0*increment
    
    i = 0
    while (i < 9):
        j = 0
        while (j < 9):
            print 'TOWER(%d,%d)=%f' % (posx+i, posy+j, output_data[ (posx + i) + (posy + j) * xsize ])
            j = j + 1
        i = i + 1
    '''

#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
MAX_VALID_POINT = 699.0


if len(sys.argv) != 3:
    print 'Usage: insert_tower_dem_geotiff.py <input DEM GeoTiff> <output DEM GeoTiff>'
    print ''
    print 'Generates a DEM GeoTiff with a tower inserted at a specified location (hard coded).'
    print 'Modify script to insert tower with proper dimensions.'
    print 'OBS: can be modified to insert other structures.'
    print ''
    exit(1)


#===============================================================================
#Load input raster file image (DEM)

dataset = gdal.Open( sys.argv[1], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
    
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'


band = dataset.GetRasterBand(1)

print 'Band Type=',gdal.GetDataTypeName(band.DataType)

min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

scandata = band.ReadRaster( 0, 0, xsize, ysize, \
                            xsize, ysize, band.DataType )
#print len(scandata)

tuple_of_bytes = struct.unpack(data_types[gdal.GetDataTypeName(band.DataType)] * band.XSize * band.YSize, scandata)

#Print first element
#print tuple_of_bytes[0]
#Print last element
#print tuple_of_bytes[band.XSize * band.YSize - 1]

output_data = array.array('f', tuple_of_bytes)
index = 0
while (index < xsize*ysize):
    output_data[index] = 0;
    index = index + 1


#===============================================================================
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)


#===============================================================================
#Add tower with proper size

E =  279407.89
N = 4272761.43
TOWER_HEIGHT = 60.0
print 'Tower coordinates: %f %f' % (E, N)
xpos = int(round((E - minE) / abs(geotransform[1])))
ypos = int(round((maxN - N) / abs(geotransform[5])))
print 'Tower position inside file: %f %f' % (xpos, ypos)
add_tower(xpos, ypos, TOWER_HEIGHT)


#===============================================================================
#Save output raster image file (DEM)

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[2], xsize, ysize, dataset.RasterCount , band.DataType )

dst_ds.SetGeoTransform( dataset.GetGeoTransform() )
dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount


#Set altitude offset - need to consider tower size
altitude_offset = 0


aux_str = ''
index = 0
while (index < xsize*ysize):
    aux_value = tuple_of_bytes[index] - altitude_offset
    aux_value = aux_value + output_data[index]
    if (aux_value < 0.0):
        aux_value = 0.0 
    #if (aux_value > 240.0):
    #    print aux_value
    #    aux_value = 255
    aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band.DataType)], aux_value)
    index = index + 1 

dst_ds.GetRasterBand(1).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(2).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(3).WriteRaster(0, 0, xsize, ysize, aux_str)



# Once we're done, close properly the datasets
dataset = None
dst_ds = None



