#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random
import math



def set_rectangle_color(len_x, len_y, bytes_r, bytes_g, bytes_b, start_x, start_y, size_x, size_y, rgb_color):
    for i in range(0, size_x):
        for j in range(0, size_y):
            bytes_r[ (start_x + i) + (start_y + j) * len_x ] = rgb_color[0]
            bytes_g[ (start_x + i) + (start_y + j) * len_x ] = rgb_color[1]
            bytes_b[ (start_x + i) + (start_y + j) * len_x ] = rgb_color[2]


#===============================================================================
#Gloval variables

RGB_BLACK = [0, 0, 0]
RGB_GREEN = [0, 128, 0]
RGB_LIME  = [0, 255, 0]

RGB_GRAY       = [128, 128, 128]
RGB_LIGHT_GRAY = [192, 192, 192]
RGB_DARK_GRAY  = [ 64,  64,  64]


#units in meters

tree_max_height = 16
tree_min_height = 4

build_max_height = 100
build_min_height = 10
build_max_area = 1.0 # 100% of land area
build_min_area = 0.2 # 20% of land area

city_max_size_radius = 4480

city_block_size = 192

max_block_divisions = 4

city_street_width = 32


output_data = None 
xsize = 0
ysize = 0


#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
MAX_VALID_POINT = 699.0


if len(sys.argv) != 4:
    print 'Usage: sim_city_raster_gen.py <input Raster GeoTiff> <output Raster GeoTiff> <output structures locations>'
    print ''
    print 'Generates a Raster GeoTiff with a auto generated city and a list of structures (text output).'
    print ''
    exit(1)


#===============================================================================
#Load input raster file image (DEM)

dataset = gdal.Open( sys.argv[1], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()


geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'


band_r = dataset.GetRasterBand(1)
band_g = dataset.GetRasterBand(2)
band_b = dataset.GetRasterBand(3)
band_a = dataset.GetRasterBand(4)

print 'R Band Type=',gdal.GetDataTypeName(band_r.DataType)
print 'G Band Type=',gdal.GetDataTypeName(band_g.DataType)
print 'B Band Type=',gdal.GetDataTypeName(band_b.DataType)
print 'A Band Type=',gdal.GetDataTypeName(band_a.DataType)

'''
min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'
'''

scandata_r = band_r.ReadRaster( 0, 0, xsize, ysize, xsize, ysize, band_r.DataType )
scandata_g = band_g.ReadRaster( 0, 0, xsize, ysize, xsize, ysize, band_g.DataType )
scandata_b = band_b.ReadRaster( 0, 0, xsize, ysize, xsize, ysize, band_b.DataType )
scandata_a = band_a.ReadRaster( 0, 0, xsize, ysize, xsize, ysize, band_a.DataType )

tuple_of_bytes_r = struct.unpack(data_types[gdal.GetDataTypeName(band_r.DataType)] * band_r.XSize * band_r.YSize, scandata_r)
tuple_of_bytes_g = struct.unpack(data_types[gdal.GetDataTypeName(band_g.DataType)] * band_g.XSize * band_g.YSize, scandata_g)
tuple_of_bytes_b = struct.unpack(data_types[gdal.GetDataTypeName(band_b.DataType)] * band_b.XSize * band_b.YSize, scandata_b)
tuple_of_bytes_a = struct.unpack(data_types[gdal.GetDataTypeName(band_a.DataType)] * band_a.XSize * band_a.YSize, scandata_a)

data_r = array.array('f', tuple_of_bytes_r)
data_g = array.array('f', tuple_of_bytes_g)
data_b = array.array('f', tuple_of_bytes_b)
data_a = array.array('f', tuple_of_bytes_a)


#for i in range(0, 100):
#    print 'RGBA: {0} {1} {2} {3}'.format(data_r[i], data_g[i], data_b[i], data_a[i])


#===============================================================================
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)


pixel_size = geotransform[1]

center_x = int(xsize / 2.0)
center_y = int(ysize / 2.0)

city_max_size_radius = int(city_max_size_radius / pixel_size)
city_block_size = int(city_block_size / pixel_size)
city_street_width = int(city_street_width / pixel_size)

num_blocks = int(2*city_max_size_radius / city_block_size)

start_x = center_x - city_max_size_radius
start_y = center_y - city_max_size_radius


#===============================================================================
#Add structures with proper size
#B;EAST;NORTH;SIZEX;SIZEY;HEIGHT  >>> buildings
#T;EAST;NORTH;SIZEX;SIZEY;HEIGHT  >>> trees


outfile = open(sys.argv[3], 'w')

for i in range(0, num_blocks):
    for j in range(0, num_blocks):
        block_px = start_x + (city_block_size + city_street_width) * i
        block_py = start_y + (city_block_size + city_street_width) * j
        dx = center_x - block_px
        dy = center_y - block_py
        dist_center = math.sqrt(dx*dx + dy*dy)
        if (dist_center < city_max_size_radius):
            diff = city_max_size_radius - dist_center
            decision = diff * random.random()
            #Decide between empty or occupied block
            if (decision > 5) or (diff > 0.2*city_max_size_radius):
                #Insert streets
                set_rectangle_color(xsize, ysize, data_r, data_g, data_b, block_px, block_py, city_block_size+city_street_width, city_street_width, RGB_BLACK)
                set_rectangle_color(xsize, ysize, data_r, data_g, data_b, block_px, block_py, city_street_width, city_block_size+city_street_width, RGB_BLACK)
                #Insert other structures
                if (decision < 20):
                    #Insert trees or plain areas
                    decision = random.random()
                    if (decision > 0.5):
                        print 'Inserting plain area (big).'
                        set_rectangle_color(xsize, ysize, data_r, data_g, data_b, block_px+city_street_width, block_py+city_street_width, city_block_size, city_block_size, RGB_LIME)
                    else:
                        print 'Inserting trees (big).'
                        set_rectangle_color(xsize, ysize, data_r, data_g, data_b, block_px+city_street_width, block_py+city_street_width, city_block_size, city_block_size, RGB_GREEN)
                        east = minE + (block_px+city_street_width) * pixel_size
                        north = maxN - (block_py+city_street_width) * pixel_size 
                        side = city_block_size * pixel_size
                        auxStr = "T;{0};{1};{2};{3};{4}\n".format(east, north, side, side, tree_max_height)
                        outfile.write(auxStr)
                        print auxStr
                else:
                    #Divide block area in small regions
                    num_divisions = int(max_block_divisions * random.random()) + 2
                    if (num_divisions > max_block_divisions):
                        num_divisions = max_block_divisions

                    div_start_x = block_px + city_street_width
                    div_start_y = block_py + city_street_width
                    div_size = city_block_size / num_divisions
                    for k in range(0, num_divisions):
                        for l in range(0, num_divisions):
                            div_posx = div_start_x + k * div_size
                            div_posy = div_start_y + l * div_size
                            decision = random.random()
                            if (decision > 0.1):
                                #Insert man made structures                                
                                print 'Inserting buildings.'
                                height_gain = (city_max_size_radius - dist_center) / city_max_size_radius
                                height = int(build_min_height + (build_max_height - build_min_height) * random.random() + 1)
                                height = int(height_gain * height)
                                used_area = int(div_size/2.0 + (div_size/2.0) * random.random() + 1)
                                decision = random.random() * (dist_center / city_max_size_radius)
                                selected_color = RGB_GRAY
                                if (decision > 0.66):                                
                                    selected_color = RGB_DARK_GRAY
                                elif (decision < 0.33):
                                    selected_color = RGB_LIGHT_GRAY
                                else:
                                    selected_color = RGB_GRAY
                                set_rectangle_color(xsize, ysize, data_r, data_g, data_b, div_posx, div_posy, div_size, div_size, selected_color)
                                east = minE + div_posx * pixel_size
                                north = maxN - div_posy * pixel_size 
                                side = div_size * pixel_size
                                auxStr = "B;{0};{1};{2};{3};{4}\n".format(east, north, side, side, height)
                                outfile.write(auxStr)
                                print auxStr
                            else:
                                #Insert trees or plain areas
                                print 'Inserting open areas.'
                                decision = random.random()
                                if (decision > 0.5):
                                    print 'Inserting trees (small).'
                                    set_rectangle_color(xsize, ysize, data_r, data_g, data_b, div_posx, div_posy, div_size, div_size, RGB_GREEN)
                                    east = minE + div_posx * pixel_size
                                    north = maxN - div_posy * pixel_size 
                                    side = div_size * pixel_size
                                    auxStr = "T;{0};{1};{2};{3};{4}\n".format(east, north, side, side, tree_max_height)
                                    outfile.write(auxStr)
                                    print auxStr
                                else:
                                    print 'Inserting plain area (small).'
                                    set_rectangle_color(xsize, ysize, data_r, data_g, data_b, div_posx, div_posy, div_size, div_size, RGB_LIME)
                        
            else:
                print 'Empty block.'


    #struct_string = '{0};{1};{2};{3};{4};{5}'.format(st_type, st_posx, st_posy, st_sizex, st_sizey, st_height)
    #outfile.write(struct_string)

outfile.close()

#exit(0)

#===============================================================================
#Save output raster image file

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[2], xsize, ysize, dataset.RasterCount , band_r.DataType )

dst_ds.SetGeoTransform( dataset.GetGeoTransform() )
dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount


for y in range(0, ysize):
    print y
    aux_str = ''
    for x in range(0, xsize):
        aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band_r.DataType)], data_r[x+ y*xsize])
    dst_ds.GetRasterBand(1).WriteRaster(0, y, xsize, 1, aux_str)

    aux_str = ''
    for x in range(0, xsize):
        aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band_g.DataType)], data_g[x+ y*xsize])
    dst_ds.GetRasterBand(2).WriteRaster(0, y, xsize, 1, aux_str)

    aux_str = ''
    for x in range(0, xsize):
        aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band_b.DataType)], data_b[x+ y*xsize])
    dst_ds.GetRasterBand(3).WriteRaster(0, y, xsize, 1, aux_str)

    aux_str = ''
    for x in range(0, xsize):
        aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band_a.DataType)], data_a[x+ y*xsize])
    dst_ds.GetRasterBand(4).WriteRaster(0, y, xsize, 1, aux_str)


# Once we're done, close properly the datasets
dataset = None
dst_ds = None


