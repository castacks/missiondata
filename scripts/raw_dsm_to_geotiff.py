#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys

gdal.UseExceptions()
MAX_VALID_POINT = 699.0

if len(sys.argv) != 3:
    print 'Usage: raw_dsm_to_geotiff.py <input ascii file> <output geotiff>'
    print ''
    print 'Generates a DSM geotiff from an ascii point map.'
    print 'Throws away points > ' + str(MAX_VALID_POINT)
    exit(1)


# Read through the file to find the dimensions
img_offset = (500000.0, 4500000.0)
img_min = None
img_max = None
img_step = [0,0]


with open(sys.argv[1]) as f:
    f.readline() # ignore the opening line
    Estart,Nstart,Ustart = map(float,f.readline().strip().split(' '))
    img_min = (Estart, Nstart)
    for l in f:
        E,N,U = map(float,l.strip().split(' '))
        if E>Estart and img_step[0] == 0:
            img_step[0] = E - Estart
        if N>Nstart and img_step[1] == 0:
            img_step[1] = N - Nstart
    
    img_max = (E,N)

print 'Min'
print img_min
print 'Max'
print img_max
print 'Step'
print img_step

img_size = [1+int((x_max - x_min)/x_step) for x_min,x_max,x_step in zip(img_min,img_max,img_step)]
rast = np.zeros(img_size,dtype=np.float32)

with open(sys.argv[1]) as f:
    f.readline() # ignore the opening line
    Elast,Nlast,Ulast = map(float,f.readline().strip().split(' '))
    rast[0,0] = Ulast
    ii = 1
    jj = 0
    for l in f:
        E,N,U = map(float,l.strip().split(' '))
        if Elast >= E:
            ii = 0
            jj += 1
        rast[ii,jj] = U
        ii += 1

# floodfill from left to fix bad points
for ii in range(rast.shape[0]):
    for jj in range(rast.shape[1]):
        if rast[ii,jj] > MAX_VALID_POINT:
            rast[ii,jj] = rast[ii-1,jj]

dst_filename = sys.argv[2]
dst_format = "GTiff"

driver = gdal.GetDriverByName( dst_format )

dst_ds = driver.Create( dst_filename, img_size[0], img_size[1], 1, gdal.GDT_Float32 )

dst_ds.SetGeoTransform( [ img_min[0]+img_offset[0], img_step[0], 0, img_max[1]+img_offset[1], 0, -img_step[1]] )
srs = osr.SpatialReference()
srs.SetUTM( 17, 1 )
srs.SetWellKnownGeogCS( 'WGS84' )
dst_ds.SetProjection( srs.ExportToWkt() )

dst_ds.GetRasterBand(1).WriteArray( rast[:,-1::-1].transpose() )

# Once we're done, close properly the dataset
dst_ds = None
