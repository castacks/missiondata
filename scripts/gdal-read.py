#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np

gdal.UseExceptions()

src_filename = 'raster/dem.tif'

dataset = gdal.Open( src_filename, GA_ReadOnly )
