#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random

    

#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
MAX_VALID_POINT = 699.0


if len(sys.argv) != 4:
    print 'Usage: convert_dem.py <input ASCII DEM ArcGis> <input DEM GeoTiff - use projection info only> <output DEM GeoTiff>'
    print ''
    print 'Merges two DEMs files'
    print ''
    exit(1)


print ''
print '========================================================================'



#===============================================================================
#Load input raster file image (DEM)

fin = open(sys.argv[1], 'r')
lines = fin.readlines()
fin.close()

print lines[0]
aux = lines[0].split()
ncols = int(aux[1])
aux = lines[1].split()
nrows = int(aux[1])
aux = lines[2].split()
xllcorner = float(aux[1])
aux = lines[3].split()
yllcorner = float(aux[1])
aux = lines[4].split()
cellsize = float(aux[1])
aux = lines[5].split()
nodatavalue = int(aux[1])

#print ncols, nrows, xllcorner, yllcorner, cellsize, nodatavalue 

print 'Input file: ', sys.argv[1] 
print 'Lower Left Corner Coordinates: ', xllcorner, yllcorner
print 'Image size: ', ncols, nrows 
print 'Resolution: ', cellsize
print '========================================================================'


#===============================================================================
#Load input raster file image (DEM)

print 'Input file: ', sys.argv[2]

dataset = gdal.Open( sys.argv[2], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
    
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'

band = dataset.GetRasterBand(1)

print 'Band Type=',gdal.GetDataTypeName(band.DataType)

min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

print '========================================================================'

'''
scandata = band.ReadRaster( 0, 0, xsize, ysize, \
                            xsize, ysize, band.DataType )
#print len(scandata)

tuple_of_bytes = struct.unpack(data_types[gdal.GetDataTypeName(band.DataType)] * band.XSize * band.YSize, scandata)

#Print first element
#print tuple_of_bytes[0]
#Print last element
#print tuple_of_bytes[band.XSize * band.YSize - 1]


output_data = array.array('B', tuple_of_bytes)
index = 0
while (index < xsize*ysize):
    output_data[index] = 0;
    index = index + 1


#===============================================================================
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)



'''

#===============================================================================
#Save output raster image file (DEM)

print "Ouput image size: %f x %f (%f meter/pixel)" % (ncols, nrows, cellsize)

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[3], ncols, nrows, 1, GDT_Float32 )

dst_ds.SetGeoTransform( [ xllcorner, cellsize, 0, yllcorner + cellsize * nrows, 0, -cellsize ] )

dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount

output_data = array.array('f')


i = 0
while (i < nrows):
    values = lines[6+i].split()
    j = 0
    while (j < ncols):
        output_data.append( float(values[j]) )
        j = j + 1
   
    i = i + 1

aux_str = ''
index = 0
while (index < nrows*ncols):
    aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(GDT_Float32)], output_data[index])
    index = index + 1 

dst_ds.GetRasterBand(1).WriteRaster(0, 0, ncols, nrows, aux_str)
'''
dst_ds.GetRasterBand(2).WriteRaster(0, 0, xsize, ysize, aux_str)
dst_ds.GetRasterBand(3).WriteRaster(0, 0, xsize, ysize, aux_str)
'''

# Once we're done, close properly the datasets
dataset = None
dst_ds = None



