#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random
import math


#===============================================================================
#Gloval variables

#tree_max_height = 15
#tree_min_height = 5

output_data = None 
xsize = 0
ysize = 0


#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
#MAX_VALID_POINT = 699.0


if len(sys.argv) != 3:
    print 'Usage: insert_object_dem_geotiff.py <input DEM GeoTiff> <output DEM GeoTiff>'
    print ''
    print 'Generates a DEM GeoTiff with an object inserted at a specified location (hard coded).'
    print 'Modify script to insert tower with proper dimensions.'
    print 'OBS: can be modified to insert other structures.'
    print ''
    exit(1)


#===============================================================================
#Load input raster file image (DEM)

dataset = gdal.Open( sys.argv[1], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
    
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'


band = dataset.GetRasterBand(1)

print 'Band Type=',gdal.GetDataTypeName(band.DataType)

min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

scandata = band.ReadRaster( 0, 0, xsize, ysize, \
                            xsize, ysize, band.DataType )
#print len(scandata)

tuple_of_bytes = struct.unpack(data_types[gdal.GetDataTypeName(band.DataType)] * band.XSize * band.YSize, scandata)

#Print first element
#print tuple_of_bytes[0]
#Print last element
#print tuple_of_bytes[band.XSize * band.YSize - 1]

output_data = array.array('f', tuple_of_bytes)
index = 0
while (index < xsize*ysize):
    output_data[index] = 0;
    index = index + 1


#===============================================================================
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)


#===============================================================================
#Add object with proper size


#N = 4271558.0 #old car position
#E =  279147.0 #old car position
#N = 4271517.0 #new car position
#E =  279207.0 #new car position
#HEIGHT = 4.0

N = 4270465.1 #bump position - flying circus
E =  263321.5 #bump position - flying circus
MIN_HEIGHT = 0.0
MAX_HEIGHT = 3.0
CUT_HEIGHT = 2.0
RADIUS = 50.0
print 'Area center coordinates: %f %f' % (E, N)
xpos_center = int(round((E - minE) / abs(geotransform[1])))
ypos_center = int(round((maxN - N) / abs(geotransform[5])))
print 'Center position inside file: %f %f' % (xpos_center, ypos_center)

#for i in range(xsize):
#    output_data[ i + ypos * xsize ] = HEIGHT

radius = int(math.fabs(RADIUS / geotransform[1]))

dx = -radius
while (dx <= radius):
    dy = -radius
    while (dy <= radius):
        d = math.sqrt(dx*dx + dy*dy)
        if (d <= radius):
            posX = xpos_center + dx
            posY = ypos_center + dy
            if (posX >= 0) and (posX < xsize) and (posY >= 0) and (posY < ysize):
                multiplication_factor = random.randint(0, 100) / 100.0
                height = (MAX_HEIGHT - MIN_HEIGHT) * multiplication_factor + MIN_HEIGHT
                if (height < CUT_HEIGHT):
                    height = 0.0
                output_data[ posX + posY * xsize ] = height
                print 'posX={0} posY={1} height={2}'.format(posX, posY, height)
        dy = dy + 1
    dx = dx + 1


#===============================================================================
#Save output raster image file (DEM)

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[2], xsize, ysize, dataset.RasterCount , band.DataType )

dst_ds.SetGeoTransform( dataset.GetGeoTransform() )
dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount


#Set altitude offset - need to consider tower size
altitude_offset = 0


aux_str = ''
index = 0
while (index < xsize*ysize):
    aux_value = tuple_of_bytes[index] - altitude_offset
    aux_value = aux_value + output_data[index]
    if (aux_value < 0.0):
        aux_value = 0.0 
    #if (aux_value > 240.0):
    #    print aux_value
    #    aux_value = 255
    aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band.DataType)], aux_value)
    index = index + 1 

dst_ds.GetRasterBand(1).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(2).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(3).WriteRaster(0, 0, xsize, ysize, aux_str)



# Once we're done, close properly the datasets
dataset = None
dst_ds = None



