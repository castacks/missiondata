#!/usr/bin/env python
from osgeo import gdal, osr
from osgeo.gdalconst import *
import numpy as np
import sys
import struct
import array
import random
import math


#===============================================================================
#Gloval variables

#tree_max_height = 15
#tree_min_height = 5

output_data = None 
xsize = 0
ysize = 0


#man made structure dimensions: Width x Length x Height (meters)
#fake_building_01 : 13.0 x 6.0 x 6.0
#fake_building_02 : 15.0 x 11.0 x 6.0
#shooting_target_long : 17:0 x 1.8 x 1.0
#shooting_target_square : 1.8 x 1.8 x 1.0


#===============================================================================

#Conversion between GDAL types and python pack types (Can't use complex integer or float!!)  
data_types ={'Byte':'B','UInt16':'H','Int16':'h','UInt32':'I','Int32':'i','Float32':'f','Float64':'d'}  

gdal.UseExceptions()
MAX_VALID_POINT = 699.0


if len(sys.argv) != 4:
    print 'Usage: insert_tower_dem_geotiff.py <input DEM GeoTiff> <structures location text> <output DEM GeoTiff>'
    print ''
    print 'Generates a DEM GeoTiff with a tower inserted at a specified location (hard coded).'
    print 'Modify script to insert tower with proper dimensions.'
    print 'OBS: can be modified to insert other structures.'
    print ''
    exit(1)


#===============================================================================
#Load input raster file image (DEM)

dataset = gdal.Open( sys.argv[1], GA_ReadOnly )

xsize = dataset.RasterXSize
ysize = dataset.RasterYSize

print 'Driver: ', dataset.GetDriver().ShortName,'/', \
                  dataset.GetDriver().LongName
print 'Size is ',xsize,'x',ysize, \
             'x',dataset.RasterCount
print 'Projection is ',dataset.GetProjection()
    
geotransform = dataset.GetGeoTransform()
if not geotransform is None:
    print 'Origin = (',geotransform[0], ',',geotransform[3],')'
    print 'Pixel Size = (',geotransform[1], ',',geotransform[5],')'


band = dataset.GetRasterBand(1)

print 'Band Type=',gdal.GetDataTypeName(band.DataType)

min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
    (min,max) = band.ComputeRasterMinMax(1)
print 'Min=%.3f, Max=%.3f' % (min,max)

if band.GetOverviewCount() > 0:
    print 'Band has ', band.GetOverviewCount(), ' overviews.'

if not band.GetRasterColorTable() is None:
    print 'Band has a color table with ', \
    band.GetRasterColorTable().GetCount(), ' entries.'

scandata = band.ReadRaster( 0, 0, xsize, ysize, \
                            xsize, ysize, band.DataType )
#print len(scandata)

tuple_of_bytes = struct.unpack(data_types[gdal.GetDataTypeName(band.DataType)] * band.XSize * band.YSize, scandata)

#Print first element
#print tuple_of_bytes[0]
#Print last element
#print tuple_of_bytes[band.XSize * band.YSize - 1]

output_data = array.array('f', tuple_of_bytes)
index = 0
while (index < xsize*ysize):
    output_data[index] = 0;
    index = index + 1


#===============================================================================
minE = geotransform[0]
maxE = geotransform[0] +  xsize * geotransform[1]
minN = geotransform[3] +  ysize * geotransform[5]
maxN = geotransform[3]
print 'LEFT/LOWER:  %.3f %.3f' % (minE, minN)
print 'RIGHT/UPPER: %.3f %.3f' % (maxE, maxN)


#===============================================================================
#Add structures with proper size
#fake_building_01 : 13.0 x 6.0 x 6.0
#fake_building_02 : 15.0 x 11.0 x 6.0
#shooting_target_long : 17:0 x 1.8 x 1.0
#shooting_target_square : 1.8 x 1.8 x 1.0

pixel_size = geotransform[1]
if (pixel_size < 0):
    pixel_size = -1 * pixel_size

with open(sys.argv[2]) as f:
    for l in f:
        tmp = l.strip().split(';')
        print tmp
        if (tmp[0] == 'Template name'):
            print 'IGNORE FIRST LINE'
        else:
            E = float(tmp[1])
            N = float(tmp[2])
            YAW = float(tmp[3])
            sin_yaw = math.sin( YAW * (math.pi/180.0) )
            cos_yaw = math.cos( YAW * (math.pi/180.0) )
            print YAW, sin_yaw, cos_yaw
            if (YAW != 0.0):
                xpos = int(round((E - minE) / abs(geotransform[1])))
                ypos = int(round((maxN - N) / abs(geotransform[5])))
                width = 0
                length = 0
                height = 0
                if (tmp[0] == '"fake_building_01"'):
                    width = 6
                    length = 13
                    height = 6
                elif (tmp[0] == '"fake_building_02"'):
                    width = 11
                    length = 15
                    height = 6
                elif (tmp[0] == '"shooting_target_long"'):
                    width = 2
                    length = 17
                    height = 1
                elif (tmp[0] == '"shooting_target_square"'):
                    width = 2
                    length = 2
                    height = 1
                elif (tmp[0] == '"building_01"'):
                    width = 15
                    length = 25
                    height = 6
                elif (tmp[0] == '"building_02"'):
                    width = 15
                    length = 15
                    height = 9
                width = int((width/pixel_size) + 1)
                #if (width < 1):
                #    width = 1
                length = int((length/pixel_size) + 1)
                #if (length < 1):
                #    length = 1   

                tmp = math.sqrt(width*width + length*length);
                offsetX =  int( (tmp * sin_yaw)/2.0 )
                offsetY = -int( (tmp * cos_yaw)/2.0 )
                for i in range(0, width):
                    ti = i + 1
                    for j in range(0, length):
                        tj = j + 1 
                        #dist = math.sqrt(ti*ti + tj*tj)
                        #dx =  int( dist * sin_yaw)
                        #dy = -int( dist * cos_yaw)
                        #dist = math.sqrt(ti*ti + tj*tj)
                        dx =  int( j * sin_yaw + i * cos_yaw)
                        dy = -int( j * cos_yaw - i * sin_yaw)
                        print dx, dy 
                        output_data[ (xpos + dx - offsetX) + (ypos + dy - offsetY) * xsize ] = 50.0
                    

'''     
N = 4271517.0 #new car position
E =  279207.0 #new car position
HEIGHT = 3.0
print 'Object coordinates: %f %f' % (E, N)
xpos = int(round((E - minE) / abs(geotransform[1])))
ypos = int(round((maxN - N) / abs(geotransform[5])))
print 'Object position inside file: %f %f' % (xpos, ypos)

output_data[ xpos + ypos * xsize ] = HEIGHT
'''

#===============================================================================
#Save output raster image file (DEM)

driver = gdal.GetDriverByName( "GTiff" )

dst_ds = driver.Create( sys.argv[3], xsize, ysize, dataset.RasterCount , band.DataType )

dst_ds.SetGeoTransform( dataset.GetGeoTransform() )
dst_ds.SetProjection( dataset.GetProjection() )

print 'Size is ',dst_ds.RasterXSize,'x',dst_ds.RasterYSize,'x',dst_ds.RasterCount


#Set altitude offset - need to consider tower size
altitude_offset = 0


aux_str = ''
index = 0
while (index < xsize*ysize):
    aux_value = tuple_of_bytes[index] - altitude_offset
    aux_value = aux_value + output_data[index]
    if (aux_value < 0.0):
        aux_value = 0.0 
    #if (aux_value > 240.0):
    #    print aux_value
    #    aux_value = 255
    aux_str = aux_str + struct.pack(data_types[gdal.GetDataTypeName(band.DataType)], aux_value)
    index = index + 1 

dst_ds.GetRasterBand(1).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(2).WriteRaster(0, 0, xsize, ysize, aux_str)
#dst_ds.GetRasterBand(3).WriteRaster(0, 0, xsize, ysize, aux_str)


# Once we're done, close properly the datasets
dataset = None
dst_ds = None



