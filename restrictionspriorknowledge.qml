<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="1.5.0-Tethys" minimumScale="1" maximumScale="1e+08" minLabelScale="1" maxLabelScale="1e+08" hasScaleBasedVisibilityFlag="0" scaleBasedLabelVisibilityFlag="0">
  <transparencyLevelInt>255</transparencyLevelInt>
  <classificationattribute>zoneType</classificationattribute>
  <uniquevalue>
    <classificationfield>zoneType</classificationfield>
    <symbol>
      <lowervalue>Ground Path</lowervalue>
      <uppervalue null="1"></uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="117" blue="56" green="136"/>
      <fillpattern>SolidPattern</fillpattern>
      <texturepath null="1"></texturepath>
    </symbol>
    <symbol>
      <lowervalue>No Fly Zone</lowervalue>
      <uppervalue>No Fly Zone</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="255" blue="0" green="12"/>
      <fillpattern>CrossPattern</fillpattern>
      <texturepath></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Obstacle</lowervalue>
      <uppervalue></uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="193" blue="0" green="22"/>
      <fillpattern>Dense5Pattern</fillpattern>
      <texturepath></texturepath>
    </symbol>
    <symbol>
      <lowervalue>Potential Landing Site</lowervalue>
      <uppervalue>Potential Landing Site</uppervalue>
      <label></label>
      <pointsymbol>hard:circle</pointsymbol>
      <pointsize>2</pointsize>
      <pointsizeunits>pixels</pointsizeunits>
      <rotationclassificationfieldname></rotationclassificationfieldname>
      <scaleclassificationfieldname></scaleclassificationfieldname>
      <symbolfieldname></symbolfieldname>
      <outlinecolor red="0" blue="0" green="0"/>
      <outlinestyle>SolidLine</outlinestyle>
      <outlinewidth>0.26</outlinewidth>
      <fillcolor red="212" blue="0" green="200"/>
      <fillpattern>BDiagPattern</fillpattern>
      <texturepath></texturepath>
    </symbol>
  </uniquevalue>
  <edittypes>
    <edittype type="0" name="maximumAlt"/>
    <edittype type="4" name="zoneType"/>
  </edittypes>
  <editform></editform>
  <editforminit></editforminit>
  <annotationform></annotationform>
  <displayfield>zoneType</displayfield>
  <label>1</label>
  <attributeactions/>
  <labelfield>zoneType</labelfield>
  <labelattributes>
    <label fieldname="zoneType" text="Label"/>
    <family fieldname="" name="Lucida Grande"/>
    <size fieldname="" units="pt" value="12"/>
    <bold fieldname="" on="0"/>
    <italic fieldname="" on="0"/>
    <underline fieldname="" on="0"/>
    <strikeout fieldname="" on="0"/>
    <color fieldname="" red="0" blue="0" green="0"/>
    <x fieldname=""/>
    <y fieldname=""/>
    <offset x="0" y="0" units="pt" yfieldname="" xfieldname=""/>
    <angle fieldname="" value="0" auto="0"/>
    <alignment fieldname="" value="center"/>
    <buffercolor fieldname="" red="255" blue="255" green="255"/>
    <buffersize fieldname="" units="pt" value="1"/>
    <bufferenabled fieldname="" on=""/>
    <multilineenabled fieldname="" on=""/>
    <selectedonly on=""/>
  </labelattributes>
  <overlay display="false" type="diagram">
    <renderer item_interpretation="linear">
      <diagramitem size="0" value="0"/>
      <diagramitem size="0" value="0"/>
    </renderer>
    <factory sizeUnits="MM" type="Pie">
      <wellknownname>Pie</wellknownname>
      <classificationfield>0</classificationfield>
    </factory>
    <scalingAttribute>0</scalingAttribute>
  </overlay>
</qgis>
