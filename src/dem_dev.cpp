/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <missiondata/dem.h>
//#include <gdal/gdal_priv.h>
//#include <missionstate.h>

DEMData::DEMData(float defaultH, int blockSize):
	heightmap(NULL),
	nXSize(0),
	nYSize(0),
	memBlockX(0),
	memBlockY(0),
	memBlockSize(blockSize),
	minValue(0),
	maxValue(0),
	defaultHeight(defaultH),
	zOffset(0.0)
{}


DEMData::~DEMData()
{
    delete[] heightmap;
    GDALClose(poDataset);
}


bool DEMData::loadFile(float zOffsetx,std::string filename)
{
    zOffset = zOffsetx;
    std:: string pszFilename =filename;
    //GDALDataset  *poDataset;
    std::cout<<"DEMData: Trying to open file \""<<pszFilename<<" orig file: "<<filename<<"\"\n";
    GDALAllRegister();
    poDataset = (GDALDataset *) GDALOpen((char *) pszFilename.c_str() , GA_ReadOnly );
    if( poDataset == NULL )
    {
        std::cout<<"Was not able to  open file \""<<pszFilename<<"\""<<std::endl;
	      return false;
    }
    else
    {
        //Meta data
        printf( "Driver: %s/%s\n",
                poDataset->GetDriver()->GetDescription(),
                poDataset->GetDriver()->GetMetadataItem( GDAL_DMD_LONGNAME ) );

        printf( "Size is %dx%dx%d\n",
                poDataset->GetRasterXSize(), poDataset->GetRasterYSize(),
                poDataset->GetRasterCount() );

        if( poDataset->GetProjectionRef()  != NULL )
            printf( "Projection is `%s'\n", poDataset->GetProjectionRef() );

        if( poDataset->GetGeoTransform( adfGeoTransform ) == CE_None )
        {
            printf( "Origin = (%.6f,%.6f)\n",
                    adfGeoTransform[0], adfGeoTransform[3] );

            printf( "Pixel Size = (%.6f,%.6f)\n",
                    adfGeoTransform[1], adfGeoTransform[5] );
            printf( "Rotation = (%.6f,%.6f)\n",
                    adfGeoTransform[2], adfGeoTransform[4] );

        }

        //Now get the actual pixels:
        GDALRasterBand  *poBand;
        int             nBlockXSize, nBlockYSize;
        int             bGotMin, bGotMax;
        double          adfMinMax[2];

        poBand = poDataset->GetRasterBand( 1 );
        poBand->GetBlockSize( &nBlockXSize, &nBlockYSize );
        printf( "Block=%dx%d Type=%s, ColorInterp=%s\n",
                nBlockXSize, nBlockYSize,
                GDALGetDataTypeName(poBand->GetRasterDataType()),
                GDALGetColorInterpretationName(
                        poBand->GetColorInterpretation()) );

        adfMinMax[0] = poBand->GetMinimum( &bGotMin );
        adfMinMax[1] = poBand->GetMaximum( &bGotMax );
        if( ! (bGotMin && bGotMax) )
            GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, adfMinMax);

        printf( "Min=%.3fd, Max=%.3f\n", adfMinMax[0], adfMinMax[1] );
        minValue = adfMinMax[0];
        maxValue = adfMinMax[1];
        if( poBand->GetOverviewCount() > 0 )
            printf( "Band has %d overviews.\n", poBand->GetOverviewCount() );

        if( poBand->GetColorTable() != NULL )
            printf( "Band has a color table with %d entries.\n",
                    poBand->GetColorTable()->GetColorEntryCount() );



        nXSize = poBand->GetXSize();
        nYSize = poBand->GetYSize();
        
        std::cout << "nXSize: " << nXSize << std::endl;
        std::cout << "nYSize: " << nYSize << std::endl;
        
        
        memBlockSize = std::min(nXSize, nYSize);
        loadMemoryAround(0, 0);
        
        /*
        delete[] heightmap;
        
        heightmap = new float[nXSize * nYSize];
        poBand->RasterIO( GF_Read, 0, 0, nXSize, nYSize,
                          heightmap, nXSize, nYSize, GDT_Float32,
                          0, 0 );

        GDALClose(poDataset);
        */
	return true;
    }

}



float DEMData::getData(const double &x,const double &y, bool *valid, int rasterBand)
{   
    if(heightmap)
    {
        int px = round((y - adfGeoTransform[0])/adfGeoTransform[1]);
        int py = round((x - adfGeoTransform[3])/adfGeoTransform[5]);
        
        // Check that the requested coordinates are on the map
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
          // Check that the requested coordiantes are loaded into memory
          if (!inMemoryBlock(px, py))
          {
            std::cout << "Load around: " << px << "," << py << std::endl;
            loadMemoryAround(px, py);
          }
          
          if(valid)
                *valid = true;  
          return -(heightmap[nXSize*py+px]+ zOffset);

        }else {
            if(valid)
                *valid = false;
            //Limit the range
            //px = min(nXSize,max(0,px));
            //py = min(nYSize,max(0,py));
            return -defaultHeight;
        }
    }else {
        if(valid)
            *valid = false;
        return -defaultHeight;
    }

}

bool DEMData::inMemoryBlock(int x, int y)
{
  bool inBlock = (x >= memBlockX && x <= (memBlockX + memBlockSize) && y >= memBlockY && y <= (memBlockY + memBlockSize));
  if (inBlock)
  {
    //std::cout << "in block! \n";
    }
  else
  {
    std::cout << x << "," << y << "," << memBlockX << "," << memBlockY << "," << memBlockX + memBlockSize << "," << memBlockY + memBlockSize << std::endl;
    }
    
  return inBlock;
}

void DEMData::loadMemoryAround(int x, int y)
{

  GDALRasterBand  *poBand;
  poBand = poDataset->GetRasterBand( 1 );
        
        
  memBlockX = (x - memBlockSize/2);
  memBlockY = (y - memBlockSize/2);
  
  int minX = std::max(0, memBlockX);
  int minY = std::max(0, memBlockY);
  int maxX = std::min(nXSize, memBlockX + memBlockSize);
  int maxY = std::min(nYSize, memBlockY + memBlockSize);  
  
  delete[] heightmap;
  heightmap = new float[memBlockSize * memBlockSize];
  poBand->RasterIO( GF_Read, minX, minY, maxX-minX, maxY-minY, heightmap, memBlockSize, memBlockSize, GDT_Float32, 0, 0 );
  
  


}

