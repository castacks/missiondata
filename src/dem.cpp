/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <missiondata/dem.h>
#include <gdal/gdal_priv.h>
//#include <gdal_priv.h>
//#include <missionstate.h>

DEMData::DEMData(float defaultH):
	heightmap(NULL),
	nXSize(0),
	nYSize(0),
	minValue(0),
	maxValue(0),
	defaultHeight(defaultH),
	zOffset(0.0)
{}

DEMData::~DEMData()
{
    delete[] heightmap;
}


bool DEMData::loadFile(float zOffsetx,std::string filename)
{
    zOffset = zOffsetx;
    std:: string pszFilename =filename;
    GDALDataset  *poDataset;
    std::cout<<"DEMData: Trying to open file \""<<pszFilename<<" orig file: "<<filename<<"\"\n";
    GDALAllRegister();
    poDataset = (GDALDataset *) GDALOpen((char *) pszFilename.c_str() , GA_ReadOnly );
    if( poDataset == NULL )
    {
        std::cout<<"Was not able to  open file \""<<pszFilename<<"\""<<std::endl;
	return false;
    }else
    {
        //Meta data
        printf( "Driver: %s/%s\n",
                poDataset->GetDriver()->GetDescription(),
                poDataset->GetDriver()->GetMetadataItem( GDAL_DMD_LONGNAME ) );

        printf( "Size is %dx%dx%d\n",
                poDataset->GetRasterXSize(), poDataset->GetRasterYSize(),
                poDataset->GetRasterCount() );

        if( poDataset->GetProjectionRef()  != NULL )
            printf( "Projection is `%s'\n", poDataset->GetProjectionRef() );

        if( poDataset->GetGeoTransform( adfGeoTransform ) == CE_None )
        {
            printf( "Origin = (%.6f,%.6f)\n",
                    adfGeoTransform[0], adfGeoTransform[3] );

            printf( "Pixel Size = (%.6f,%.6f)\n",
                    adfGeoTransform[1], adfGeoTransform[5] );
            printf( "Rotation = (%.6f,%.6f)\n",
                    adfGeoTransform[2], adfGeoTransform[4] );

        }

        //Now get the actual pixels:
        GDALRasterBand  *poBand;
        int             nBlockXSize, nBlockYSize;
        int             bGotMin, bGotMax;
        double          adfMinMax[2];

        poBand = poDataset->GetRasterBand( 1 );
        poBand->GetBlockSize( &nBlockXSize, &nBlockYSize );
        printf( "Block=%dx%d Type=%s, ColorInterp=%s\n",
                nBlockXSize, nBlockYSize,
                GDALGetDataTypeName(poBand->GetRasterDataType()),
                GDALGetColorInterpretationName(
                        poBand->GetColorInterpretation()) );

        adfMinMax[0] = poBand->GetMinimum( &bGotMin );
        adfMinMax[1] = poBand->GetMaximum( &bGotMax );
        if( ! (bGotMin && bGotMax) )
            GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, adfMinMax);

        printf( "Min=%.3fd, Max=%.3f\n", adfMinMax[0], adfMinMax[1] );
        minValue = adfMinMax[0];
        maxValue = adfMinMax[1];
        if( poBand->GetOverviewCount() > 0 )
            printf( "Band has %d overviews.\n", poBand->GetOverviewCount() );

        if( poBand->GetColorTable() != NULL )
            printf( "Band has a color table with %d entries.\n",
                    poBand->GetColorTable()->GetColorEntryCount() );



        nXSize = poBand->GetXSize();
        nYSize = poBand->GetYSize();
        printf("XSize=%d, YSize=%d\n", nXSize, nYSize);
        
        delete[] heightmap;
        heightmap = new float[nXSize * nYSize];
        poBand->RasterIO( GF_Read, 0, 0, nXSize, nYSize,
                          heightmap, nXSize, nYSize, GDT_Float32,
                          0, 0 );

        GDALClose(poDataset);
	return true;
    }

}

bool DEMData::writeFile(std::string filename)
{
	std::string pszDstFilename =filename;

	const char *pszFormat = "GTiff";
	GDALDriver *poDriver;
	//char **papszMetadata;

	poDriver = GetGDALDriverManager()->GetDriverByName(pszFormat);

	if( poDriver == NULL )
		exit( 1 );

    GDALDataset *poDstDS;
    char **papszOptions = NULL;

    poDstDS = poDriver->Create( (char *) pszDstFilename.c_str(), nXSize, nYSize, 1, GDT_Float32,
                                papszOptions );

  /*  OGRSpatialReference oSRS;
    char *pszSRS_WKT = NULL;*/
    GDALRasterBand *poBand;


    poDstDS->SetGeoTransform( adfGeoTransform );
/*
    oSRS.SetUTM( 11, TRUE );
    oSRS.SetWellKnownGeogCS( "NAD27" );
    oSRS.exportToWkt( &pszSRS_WKT );
    poDstDS->SetProjection( pszSRS_WKT );
    CPLFree( pszSRS_WKT );*/

    poBand = poDstDS->GetRasterBand(1);
    poBand->RasterIO( GF_Write, 0, 0, nXSize, nYSize,
                      heightmap, nXSize, nYSize,  GDT_Float32, 0, 0 );

    /* Once we're done, close properly the dataset */
    GDALClose( (GDALDatasetH) poDstDS );
    
    return true;
}

bool DEMData::setObject(DEMData *imitate)
{
	delete[] heightmap;

	adfGeoTransform[1] = imitate->getXIncrement();
	adfGeoTransform[5] = imitate->getYIncrement();
	nXSize = imitate->getXSize();
	nYSize = imitate->getYSize();
	adfGeoTransform[0] = imitate->xOrigin();
	adfGeoTransform[3] = imitate->yOrigin();
	adfGeoTransform[2] = 0;
	adfGeoTransform[4] = 0;
	heightmap = new float[nXSize * nYSize];

  return true;
}

void DEMData::setHeight(const double &x,const double &y, float height)
{
    if(heightmap)
    {
        int px = round((y - adfGeoTransform[0])/adfGeoTransform[1]);
        int py = round((x - adfGeoTransform[3])/adfGeoTransform[5]);
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
            heightmap[nXSize*py+px] = -height-zOffset;

        }
    }
}
float DEMData::getHeight(const double &x,const double &y, bool *valid)
{
    if(heightmap)
    {
        int px = round((y - adfGeoTransform[0])/adfGeoTransform[1]);
        int py = round((x - adfGeoTransform[3])/adfGeoTransform[5]);
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
            if(valid)
                *valid = true;
            return -(heightmap[nXSize*py+px]+ zOffset);

        }else {
            if(valid)
                *valid = false;
            //Limit the range
            //px = min(nXSize,max(0,px));
            //py = min(nYSize,max(0,py));
            return -defaultHeight;
        }
    }else {
        if(valid)
            *valid = false;
        return -defaultHeight;
    }

}

float DEMData::interpHeight(const double &x,const double &y, bool *valid)
{
    if(heightmap)
    {
        double px = ((y - adfGeoTransform[0])/adfGeoTransform[1]);
        double py = ((x - adfGeoTransform[3])/adfGeoTransform[5]);
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
            if(valid)
                *valid = true;
            int x1 = floor(px);
            int x2 = ceil(px);
            int y1 = floor(py);
            int y2 = ceil(py);
    				double z11 = -(heightmap[nXSize*y1+x1]+ zOffset);
    				double z12 = -(heightmap[nXSize*y2+x1]+ zOffset);
    				double z21 = -(heightmap[nXSize*y1+x2]+ zOffset);
    				double z22 = -(heightmap[nXSize*y2+x2]+ zOffset);
    				
            return bilinearInterp(px, py, x1, x2, y1, y2, z11, z12, z21, z22);

        }else {
            if(valid)
                *valid = false;
            //Limit the range
            //px = min(nXSize,max(0,px));
            //py = min(nYSize,max(0,py));
            return -defaultHeight;
        }
    }else {
        if(valid)
            *valid = false;
        return -defaultHeight;
    }

}

double DEMData::bilinearInterp(double x, double y, double x1, double x2, double y1, double y2, double z11, double z12, double z21, double z22)
{
	double z;
	double t = (x2-x1)*(y2-y1);
	
	z = (z11/t)*(x2-x)*(y2-y) +
			(z21/t)*(x-x1)*(y2-y) +
			(z12/t)*(x2-x)*(y-y1) +
			(z22/t)*(x-x1)*(y-y1);
			
	return z;
}

