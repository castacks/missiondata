/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <missiondata/overlay.h>
#include <gdal/gdal_priv.h>
//#include <gdal_priv.h>
//#include <missionstate.h>

OverlayData::OverlayData():
	rChannel(NULL),
	gChannel(NULL),
	bChannel(NULL),
	nXSize(0),
	nYSize(0),
	minValue(0),
	maxValue(0)
{}


OverlayData::~OverlayData()
{
    delete[] rChannel;
    delete[] gChannel;
    delete[] bChannel;
}


bool OverlayData::loadFile(std::string filename)
{

    std:: string pszFilename =filename;
    GDALDataset  *poDataset;
    std::cout<<"OverlayData: Trying to open file \""<<pszFilename<<" orig file: "<<filename<<"\"\n";
    GDALAllRegister();
    poDataset = (GDALDataset *) GDALOpen((char *) pszFilename.c_str() , GA_ReadOnly );
    if( poDataset == NULL )
    {
        std::cout<<"Was not able to  open file \""<<pszFilename<<"\""<<std::endl;
	return false;
    }else
    {
        //Meta data
        printf( "Driver: %s/%s\n",
                poDataset->GetDriver()->GetDescription(),
                poDataset->GetDriver()->GetMetadataItem( GDAL_DMD_LONGNAME ) );

        printf( "Size is %dx%dx%d\n",
                poDataset->GetRasterXSize(), poDataset->GetRasterYSize(),
                poDataset->GetRasterCount() );

        if( poDataset->GetProjectionRef()  != NULL )
            printf( "Projection is `%s'\n", poDataset->GetProjectionRef() );

        if( poDataset->GetGeoTransform( adfGeoTransform ) == CE_None )
        {
            printf( "Origin = (%.6f,%.6f)\n",
                    adfGeoTransform[0], adfGeoTransform[3] );

            printf( "Pixel Size = (%.6f,%.6f)\n",
                    adfGeoTransform[1], adfGeoTransform[5] );
            printf( "Rotation = (%.6f,%.6f)\n",
                    adfGeoTransform[2], adfGeoTransform[4] );

        }


        //Now get the actual pixels:
        GDALRasterBand  *poBand;
        int             nBlockXSize, nBlockYSize;
        int             bGotMin, bGotMax;
        double          adfMinMax[2];

        poBand = poDataset->GetRasterBand( 1 );
        poBand->GetBlockSize( &nBlockXSize, &nBlockYSize );
        printf( "Block=%dx%d Type=%s, ColorInterp=%s\n",
                nBlockXSize, nBlockYSize,
                GDALGetDataTypeName(poBand->GetRasterDataType()),
                GDALGetColorInterpretationName(
                        poBand->GetColorInterpretation()) );

        adfMinMax[0] = poBand->GetMinimum( &bGotMin );
        adfMinMax[1] = poBand->GetMaximum( &bGotMax );
        if( ! (bGotMin && bGotMax) )
            GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, adfMinMax);

        printf( "Min=%.3fd, Max=%.3f\n", adfMinMax[0], adfMinMax[1] );
        minValue = adfMinMax[0];
        maxValue = adfMinMax[1];
        if( poBand->GetOverviewCount() > 0 )
            printf( "Band has %d overviews.\n", poBand->GetOverviewCount() );

        if( poBand->GetColorTable() != NULL )
            printf( "Band has a color table with %d entries.\n",
                    poBand->GetColorTable()->GetColorEntryCount() );



        nXSize = poBand->GetXSize();
        nYSize = poBand->GetYSize();
        delete[] rChannel;
        rChannel = new char[nXSize * nYSize];
        poBand->RasterIO( GF_Read, 0, 0, nXSize, nYSize,
                          rChannel, nXSize, nYSize, poBand->GetRasterDataType(),
                          0, 0 );

////////////////////////////////////////////////////////////

        poBand = poDataset->GetRasterBand( 2 );
        poBand->GetBlockSize( &nBlockXSize, &nBlockYSize );
        printf( "Block=%dx%d Type=%s, ColorInterp=%s\n",
                nBlockXSize, nBlockYSize,
                GDALGetDataTypeName(poBand->GetRasterDataType()),
                GDALGetColorInterpretationName(
                        poBand->GetColorInterpretation()) );

        adfMinMax[0] = poBand->GetMinimum( &bGotMin );
        adfMinMax[1] = poBand->GetMaximum( &bGotMax );
        if( ! (bGotMin && bGotMax) )
            GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, adfMinMax);

        printf( "Min=%.3fd, Max=%.3f\n", adfMinMax[0], adfMinMax[1] );
        minValue = adfMinMax[0];
        maxValue = adfMinMax[1];
        if( poBand->GetOverviewCount() > 0 )
            printf( "Band has %d overviews.\n", poBand->GetOverviewCount() );

        if( poBand->GetColorTable() != NULL )
            printf( "Band has a color table with %d entries.\n",
                    poBand->GetColorTable()->GetColorEntryCount() );



        nXSize = poBand->GetXSize();
        nYSize = poBand->GetYSize();
        delete[] gChannel;
        gChannel = new char[nXSize * nYSize];
        poBand->RasterIO( GF_Read, 0, 0, nXSize, nYSize,
                          gChannel, nXSize, nYSize, poBand->GetRasterDataType(),
                          0, 0 );

////////////////////////////////////////////////////////////

        poBand = poDataset->GetRasterBand( 3 );
        poBand->GetBlockSize( &nBlockXSize, &nBlockYSize );
        printf( "Block=%dx%d Type=%s, ColorInterp=%s\n",
                nBlockXSize, nBlockYSize,
                GDALGetDataTypeName(poBand->GetRasterDataType()),
                GDALGetColorInterpretationName(
                        poBand->GetColorInterpretation()) );

        adfMinMax[0] = poBand->GetMinimum( &bGotMin );
        adfMinMax[1] = poBand->GetMaximum( &bGotMax );
        if( ! (bGotMin && bGotMax) )
            GDALComputeRasterMinMax((GDALRasterBandH)poBand, TRUE, adfMinMax);

        printf( "Min=%.3fd, Max=%.3f\n", adfMinMax[0], adfMinMax[1] );
        minValue = adfMinMax[0];
        maxValue = adfMinMax[1];
        if( poBand->GetOverviewCount() > 0 )
            printf( "Band has %d overviews.\n", poBand->GetOverviewCount() );

        if( poBand->GetColorTable() != NULL )
            printf( "Band has a color table with %d entries.\n",
                    poBand->GetColorTable()->GetColorEntryCount() );



        nXSize = poBand->GetXSize();
        nYSize = poBand->GetYSize();
        delete[] bChannel;
        bChannel = new char[nXSize * nYSize];
        poBand->RasterIO( GF_Read, 0, 0, nXSize, nYSize,
                          bChannel, nXSize, nYSize, poBand->GetRasterDataType(),
                          0, 0 );



        GDALClose(poDataset);
	return true;
    }

}

char OverlayData::getRChannel(const double &x,const double &y, bool *valid)
{
    if(rChannel)
    {
        int px = round((y - adfGeoTransform[0])/adfGeoTransform[1]);
        int py = round((x - adfGeoTransform[3])/adfGeoTransform[5]);
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
            if(valid)
                *valid = true;
            return -(rChannel[nXSize*py+px]);

        }else {
            if(valid)
                *valid = false;
            //Limit the range
            //px = min(nXSize,max(0,px));
            //py = min(nYSize,max(0,py));
            return 1.0;
        }
    }else {
        if(valid)
            *valid = false;
        return 1.0;
    }

}

char OverlayData::getGChannel(const double &x,const double &y, bool *valid)
{
    if(rChannel)
    {
        int px = round((y - adfGeoTransform[0])/adfGeoTransform[1]);
        int py = round((x - adfGeoTransform[3])/adfGeoTransform[5]);
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
            if(valid)
                *valid = true;
            return -(gChannel[nXSize*py+px]);

        }else {
            if(valid)
                *valid = false;
            //Limit the range
            //px = min(nXSize,max(0,px));
            //py = min(nYSize,max(0,py));
            return 1.0;
        }
    }else {
        if(valid)
            *valid = false;
        return 1.0;
    }

}

char OverlayData::getBChannel(const double &x,const double &y, bool *valid)
{
    if(rChannel)
    {
        int px = round((y - adfGeoTransform[0])/adfGeoTransform[1]);
        int py = round((x - adfGeoTransform[3])/adfGeoTransform[5]);
        if((px>=0)&&(px<nXSize)&&(py>=0)&&(py<nYSize))
        {
            if(valid)
                *valid = true;
            return -(bChannel[nXSize*py+px]);

        }else {
            if(valid)
                *valid = false;
            //Limit the range
            //px = min(nXSize,max(0,px));
            //py = min(nYSize,max(0,py));
            return 1.0;
        }
    }else {
        if(valid)
            *valid = false;
        return 1.0;
    }

}


