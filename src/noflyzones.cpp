/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/*s
 * noflyzones.cpp
 *
 *  Created on: Jul 7, 2011
 *      Author: maasted
 */

#include "missiondata/noflyzones.h"
#define LAYER_NAME "landingsites"
#define LABEL_FEAT_IND (0)
#define MAX_ALT_FEAT_IND (1)

//make verbose
//#define DEBUG

NoFlyZones::NoFlyZones(const char * shpFile) {
	OGRRegisterAll();
	printf("Loading No Fly Zones from shapefile %s.\n",shpFile);
	this->poDS = OGRSFDriverRegistrar::Open(shpFile, FALSE );
	if( poDS == NULL )
	{
		fprintf(stderr,"Open failed on shapefile %s.\n",shpFile);
		exit( 1 );
	}

	OGRLayer  *poLayer;
	poLayer = poDS->GetLayerByName(LAYER_NAME);
	if (poLayer == NULL) {
		fprintf(stderr,"Shape file %s does not have layer named \"%s\"\n",shpFile,LAYER_NAME);
		exit( 1 );
	}

	poLayer->ResetReading();

	OGRFeature *poFeature;
	NoFlyZone thisNoFly;
	this->noFlyZone_vec.clear();

	int iFeature = 0;
	/* Extract all of the relevant features. */
	while( (poFeature = poLayer->GetNextFeature()) != NULL )
	{
		++iFeature;
		thisNoFly.feat = poFeature;
#ifdef DEBUG
		printf("Examining feature %d\n",iFeature);
#endif
		OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();
		OGRFieldDefn *poFieldDefn = poFDefn->GetFieldDefn(LABEL_FEAT_IND);
		if (poFieldDefn == NULL || poFieldDefn->GetType() != OFTString) {
#ifdef DEBUG
			fprintf(stderr,"Skipping feature - label metadata field was not string.\n");
#endif
			continue;
		}
		if(0==strcmp(poFeature->GetFieldAsString(LABEL_FEAT_IND),"No Fly Zone")) {
#ifdef DEBUG
			printf("Found no fly zone!\n");
#endif
		} else {
			continue;
		}

		poFieldDefn = poFDefn->GetFieldDefn(MAX_ALT_FEAT_IND);
		if (poFieldDefn == NULL || poFieldDefn->GetType() != OFTReal) {
			fprintf(stderr,"Noflyzone doesn't have a max alt! Exiting!\n");
			exit(1);
		}
		thisNoFly.alt_ceiling_m = poFeature->GetFieldAsDouble(MAX_ALT_FEAT_IND);
#ifdef DEBUG
		printf("Altitude ceiling: %f\n",thisNoFly.alt_ceiling_m);
#endif
		OGRGeometry *poGeometry;
		poGeometry = poFeature->GetGeometryRef();
		if (poGeometry == NULL) {
			fprintf(stderr,"No fly zone has null geometry! Exiting\n");
			exit(1);
		}
		if (wkbFlatten(poGeometry->getGeometryType()) == wkbPolygon) {
#ifdef DEBUG
			printf("No fly zone has polygon geometry!\n");
#endif
		} else {
#ifdef DEBUG
			printf("No fly zone has non-polygon geometry! Skipping.\n");
#endif
			continue;
		}
		this->noFlyZone_vec.push_back(thisNoFly);
	}
}
NoFlyZones::~NoFlyZones() {
	OGRDataSource::DestroyDataSource(this->poDS );
}

bool NoFlyZones::intersects(const CA::Plan & plan) {
	OGRLineString l;
	const std::vector<CA::State, Eigen::aligned_allocator<CA::Vector3D> > t = plan.command.getLinear();
	std::vector<CA::State,Eigen::aligned_allocator<CA::Vector3D> >::const_iterator it0,it1;
	it0 = t.begin();
	it1 = t.begin();
	it1++;
	/* For each line segment in the trajectory, build an OGR line and test it
	 * for a collision. We could build one long linestring, but to be altitude aware
	 * we have to do it this way.
	 */
	for(;it1 != t.end(); (it0++,it1++)) {
		l.empty();
		l.addPoint((*it0).pose.position_m[1],
				(*it0).pose.position_m[0]);
		l.addPoint((*it1).pose.position_m[1],
				(*it1).pose.position_m[0]);
		//DEBUGGING
		//OGRPoint p0,p1;
		//l.StartPoint(&p0);
		//l.EndPoint(&p1);
		//std::cout <<"Testing line from " << p0.getX() << "," << p0.getY() << " to " << p1.getX() <<","<< p1.getY() << std::endl;
		//END DEBUGGING
		for (std::vector<NoFlyZone>::iterator poly = this->noFlyZone_vec.begin();
				poly != this->noFlyZone_vec.end(); poly++) {
			//(*poly).feat->GetGeometryRef()->Centroid(&p0);
			//std::cout<<"Testing a polygon with centroid: " <<p0.getX()<<","<<p0.getY() <<std::endl;
			/* If we're above the polygon, skip it */
			if ((*poly).alt_ceiling_m < -(*it0).pose.position_m[2] &&
					(*poly).alt_ceiling_m < -(*it1).pose.position_m[2]) {
				std::cout<<"We are above the no fly zone"<<std::endl;
				continue;
			}
			if ((poly->feat->GetGeometryRef()->Intersects(&l))) {
				return true;
			}
		}
	}
	return false;
}
