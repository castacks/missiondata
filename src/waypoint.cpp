/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


/***************************************************************************
 *   Copyright (C) 2005 by Sebastian Scherer                               *
 *   basti@andrew.cmu.edu                                                  *
 *                                                                         *
****************************************************************************/

//#include <qgl.h>
#include <missiondata/waypoint.h>
#include <cmath>
using namespace CA;
using namespace std;

/*Waypoint Waypoint::interpolateWaypoint(const Waypoint &from,const Waypoint &to,double &perc)
{
    perc = min(1.0,max(0.0, perc));

    double percT = 1.0 - perc;
    Waypoint result;

    result.m_pos=  from.m_pos * perc + to.m_pos * percT;
    result.m_speed = from.m_speed * perc + to.m_speed * percT;

    double h1 = math_tools::limit2pi( from.m_heading);
    double h2 = math_tools::limit2pi(to.m_heading);
    double dh = math_tools::turnDirection(h1,h2);
    double ih = dh* percT + h2;
    result.m_heading = ih;
    if(perc<=0.5)
    {
        result.m_extra1  = from.m_extra1;
        result.m_extra2  = from.m_extra2;
        //result.m_heading = from.m_heading;
        result.m_wptype  = from.m_wptype;
    }
    else
    {
        result.m_extra1  = to.m_extra1;
        result.m_extra2  = to.m_extra2;
       // result.m_heading = to.m_heading;
        result.m_wptype  = to.m_wptype;

    }
    return result;
}*/

/*void Waypoint::drawWaypoint(TVector3D offset)
{
  glPushMatrix();
  TVector3D diff;diff = m_pos - offset;
  glTranslateV(diff);
  GLUquadric *sphereObj = gluNewQuadric();
  double m_radius = 0.08;
  switch(m_wptype)
  {
    case WAYPOINT_INTERMEDIATE:
      m_radius = 0.04;
      break;
    case WAYPOINT_INVALID:
      m_radius = 0.04;
      break;
    default:break;
  }
  
  if(sphereObj)
        gluSphere(sphereObj, m_radius, 8, 8);
  gluDeleteQuadric(sphereObj);
  glPopMatrix();
}*/

bool Waypoint::operator ==(const Waypoint& b) const
{
    return this->m_wptype == b.m_wptype && this->m_pos[0] == b.m_pos[0] && this->m_pos[1] == b.m_pos[1] && this->m_pos[2] == b.m_pos[2]  && this->m_heading == b.m_heading && this->m_speed == b.m_speed;
}
bool Waypoint::operator !=(const Waypoint& b) const
{
    return !(b == *this);
}

bool Waypoint::isValid()
{
 
  if(m_wptype == WP_INVALID)
    return false;
  else
    {
      if(!isinf(m_heading) && !isinf(m_speed) && !isinf(m_pos[0]) && !isinf(m_pos[1]) && !isinf(m_pos[2]))
	{ 
	  return true;
	}
      else
	return false;
      
    }
}

Waypoint & Waypoint::operator =(const Waypoint & p)
{
  this->m_wptype = p.m_wptype;
  this->m_pos = p.m_pos;
  this->m_heading = p.m_heading;
  this->m_speed = p.m_speed;
  return *this;
}

Waypoint::Waypoint(const Waypoint & p )
{
*this = p;
}

ca_common::MissionWaypoint CA::msgc(const Waypoint &wp)
{
  ca_common::MissionWaypoint wpr;
  wpr.position_m = msgcp(wp.position());
  wpr.heading_rad = wp.heading();
  wpr.speed_mps = wp.speed();
  wpr.wptype = wp.wptype();
  return wpr;
}

Waypoint  CA::msgc(const ca_common::MissionWaypoint &wp)
{
  return Waypoint((WP_TYPE)wp.wptype,msgc(wp.position_m),wp.speed_mps,wp.heading_rad);
}
