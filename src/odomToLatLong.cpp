/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


#include <iostream>
#include <iomanip>
#include "ros/ros.h"
#include <nav_msgs/Odometry.h>
#include <Eigen/Eigen>
#include <fstream>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>

typedef Eigen::Matrix<double,2,1> Vector2D;
//std::string ned_frame,odom_frame;
//tf::TransformListener listener;
std::fstream resfile;


Vector2D convertLatLong(double easting, double northing)
{
  std::string dest_frame;
  dest_frame = "+proj=latlon +datum=WGS84";
  double lon = northing;
  double lat = easting;
  Vector2D ret;
  ret(0) = lat*180.0/M_PI;
  ret(1) = lon*180.0/M_PI;
  
  return ret;
}

void odomCallback(const nav_msgs::Odometry::ConstPtr& odomMsg){
ROS_INFO("callback");
	double 	ned_X0, ned_Y0;
//	tf::StampedTransform t;
//	try {
//		listener.waitForTransform (ned_frame, odom_frame, ros::Time::now(), ros::Duration(.5)); 
//		listener.lookupTransform  (ned_frame, odom_frame, ros::Time::now(), t); 
//	}
//	catch (tf::TransformException ex){
//		ROS_ERROR("%s",ex.what());
//		ROS_ERROR_STREAM("rivermapping.updatePose: failed to find transform: " << ned_frame << " -> " << odom_frame);
//	}
//	Eigen::Affine3d t_eigen;
//	tf::TransformTFToEigen(t, t_eigen);
	ned_X0=odomMsg->pose.pose.position.x;
	ned_Y0=odomMsg->pose.pose.position.y;
	Vector2D LatLong=convertLatLong(ned_X0,ned_Y0);
	resfile << std::setprecision(15) << LatLong(0) << ", " << std::setprecision(15) <<  LatLong(1) << std::endl;	
}
int main(int argc, char **argv)
{
  ROS_INFO("odom2latlong");
  ros::init(argc, argv, "odom2latlong");
  ros::NodeHandle n;
  ros::NodeHandle np("~");
  ros::Subscriber sub1 = n.subscribe("/odom2latlong/odom", 10, odomCallback);
  std::string filefolder;
  char filename[128];
  np.param<std::string>("filefolder", filefolder, "~/");
  sprintf(filename,"%s/odomLatLong.txt", filefolder.c_str());
  resfile.open(filename, std::ios::out);
  ros::spin();
  resfile.close();
  return 0;
}
