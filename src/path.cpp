/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/


//Qt includes
//#include "qgl.h"
//#include <QFile>
//#include <QTextStream>
//#include <QMutex>
//#include <cmath>

//Our includes
//#include <math_tools.h>
//#include <waypointinput.h>
//#include <customdefs.h>
//#include <circle.h>
//#include <rmlparser.h>

#include <missiondata/path.h>
#include <missiondata/waypoint.h>
#include <ca_common/math.h>
#include <ca_common/MissionWaypoint.h>
#include <limits>

using namespace CA;

Vector3D Path::getMinVectorUpTo(const double &dist,const Vector3D &forv,double *distalong) 
{

    Vector3D result(0,0,0);
    std::vector<Waypoint>::iterator it,it_end = m_path.end();
    if(distalong)
        *distalong = 0;
    if(m_path.size()>=2)
    {
        Vector3D pos1((*m_path.begin()).position()),pos2;

        double totaldist = 0;
        double mindist = 1e20;
        for ( it = m_path.begin()+1; it != it_end; ++it )
        {

            Vector3D pos2((*it).position());
            bool isinside;
            double rv;
            Vector3D vecline(math_tools::vectorToLineSegment(pos1,pos2,forv,&isinside,&rv));
            double distance = math_tools::Length(vecline);
            if(distance<mindist)
            {
                result = vecline;
                mindist = distance;
                if(distalong)
                    *distalong = totaldist + math_tools::Length(pos2,pos1)*rv;
            }
            totaldist += math_tools::Length(pos1,pos2);
            if(totaldist>dist)
                break;
            pos1 = pos2;
        }
    }
    return result;
}



ca_common::Mission CA::msgc(const Path &path)
{
  std::vector<ca_common::MissionWaypoint> respath;
  std::vector<Waypoint> itpath;
  
  respath.reserve(itpath.size());
  BOOST_FOREACH(Waypoint wp, itpath)
    {
      respath.push_back(msgc(wp));
    }
  ca_common::Mission result;
  result.missionname = path.getPathName();
  result.missiontype = path.getPathType();
  return result;
}


Path  CA::msgc(const ca_common::Mission &path)
{
  std::vector<Waypoint> respath;
  respath.reserve(path.path.size());
  BOOST_FOREACH(ca_common::MissionWaypoint wp, path.path)
    {
      respath.push_back(msgc(wp));
    }
  Path result(respath,path.missionname,(Path::PATHTYPE)path.missiontype);
  return result;
}


Path::PATHTYPE Path::getPathType() const {return m_pathtype;}
std::string Path::getPathName() const {return m_pathname;}
std::vector<Waypoint> Path::getPath() const {return m_path;}


void Path::setPathName(const std::string &pathname)
  {m_pathname = pathname;}
void Path::setPath(std::vector<Waypoint> path){m_path = path;}
void Path::setPathType(const PATHTYPE &pt)
{m_pathtype = pt;}
