#ifndef _OVERLAY_H
#define _OVERLAY_H

#include <ca_common/math.h>
#include <string>
#include <vector>
//#include <sensorprocess.h>
//#include <customdefs.h>
//#include <workerthread.h>

class OverlayData
{
public:
	OverlayData();
	~OverlayData();
    bool loadFile(std::string filename);
    char getRChannel(const double &x,const double &y, bool *valid=NULL);
    char getGChannel(const double &x,const double &y, bool *valid=NULL);
    char getBChannel(const double &x,const double &y, bool *valid=NULL);

    double getXIncrement()
    {
        return adfGeoTransform[1];
    }
    double getYIncrement()
    {
        return adfGeoTransform[5];
    }
    int getXSize()
    {
        return nXSize;
    }
    int getYSize()
    {
        return nYSize;
    }
  double xOrigin()
  {
    return adfGeoTransform[0];
  }
  double yOrigin()
  {
    return adfGeoTransform[3];
  }
private:
	char *rChannel;
	char *gChannel;
	char *bChannel;
	int nXSize;
	int nYSize;
	char minValue;
	char maxValue;
	double adfGeoTransform[6];
	
};

#endif

