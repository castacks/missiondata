#ifndef _DEM_H
#define _DEM_H

#include <ca_common/math.h>
#include <string>
//#include <sensorprocess.h>
//#include <customdefs.h>
//#include <workerthread.h>

class DEMData
{
public:
	DEMData(float defaultH=0.0);
	~DEMData();
    bool loadFile(float zOffset,std::string filename);
    bool writeFile(std::string filename);
    bool setObject(DEMData *imitate);
	float getHeight(const double &x,const double &y, bool *valid=NULL);
	void setHeight(const double &x,const double &y, float height);
	double bilinearInterp(double x, double y, double x1, double x2, double y1, double y2, double z11, double z12, double z21, double z22);
	float interpHeight(const double &x,const double &y, bool *valid);
	void setdefaultHeight(const float &defaultH)
	{ 
		defaultHeight = defaultH;
	};
    double getXIncrement()
    {
        return adfGeoTransform[1];
    }
    double getYIncrement()
    {
        return adfGeoTransform[5];
    }
    int getXSize()
    {
        return nXSize;
    }
    int getYSize()
    {
        return nYSize;
    }
  double xOrigin()
  {
    return adfGeoTransform[0];
  }
  double yOrigin()
  {
    return adfGeoTransform[3];
  }
private:
	float *heightmap;
	int nXSize;
	int nYSize;
	float minValue;
	float maxValue;
	float defaultHeight;
	float zOffset;
	double adfGeoTransform[6];
	
};

#endif

