#ifndef _DEM_H
#define _DEM_H

#include <ca_common/math.h>
#include <string>
#include <gdal/gdal_priv.h>
//#include <sensorprocess.h>
//#include <customdefs.h>
//#include <workerthread.h>

class DEMData
{
public:
	DEMData(float defaultH=0.0, int blockSize = 200);
	~DEMData();
    bool loadFile(float zOffset,std::string filename);
	  float getHeight(const double &x,const double &y, bool *valid=NULL);
	  bool inMemoryBlock(int x, int y);
	  void loadMemoryAround(int x, int y);
    double getXIncrement()
    {
        return adfGeoTransform[1];
    }
    double getYIncrement()
    {
        return adfGeoTransform[5];
    }
    int getXSize()
    {
        return nXSize;
    }
    int getYSize()
    {
        return nYSize;
    }
  double xOrigin()
  {
    return adfGeoTransform[0];
  }
  double yOrigin()
  {
    return adfGeoTransform[3];
  }
private:
	float *bandData;
	int nXSize;
	int nYSize;
	int memBlockX;
	int memBlockY;
	int memBlockSize;
	float minValue;
	float maxValue;
	float defaultData;
	float zOffset;
	double adfGeoTransform[6];
	GDALDataset  *poDataset;
	
};

#endif

