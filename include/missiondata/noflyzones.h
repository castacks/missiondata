/*
 * noflyzones.h
 *
 *  Created on: Jul 7, 2011
 *      Author: maasted
 */

#ifndef NOFLYZONES_H_
#define NOFLYZONES_H_

#include <ca_common/math.h>
#include "gdal/ogrsf_frmts.h"

class NoFlyZones {

public:
	NoFlyZones(const char * shpFile);
	~NoFlyZones(void);
	bool intersects(const CA::Plan &t);

private:
	struct NoFlyZone {
		double alt_ceiling_m;
		OGRFeature * feat;
	};
	OGRDataSource * poDS;
    std::vector<NoFlyZone> noFlyZone_vec;
};

#endif /* NOFLYZONES_H_ */
