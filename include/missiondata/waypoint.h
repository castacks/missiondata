#ifndef _WAYPOINT_H_
#define _WAYPOINT_H_

#include <ca_common/math.h>
#include <ca_common/messagemap.h>
#include <ca_common/MissionWaypoint.h>
#include <string>

namespace CA
{

/**
* Waypoint Definition
*
*/
class Waypoint
{
protected:
    WP_TYPE m_wptype;
    Vector3D  m_pos;
    double m_heading;
    double m_speed;

public:
  Waypoint():
    m_wptype(WP_INVALID),
    m_pos(0,0,0),
    m_heading(0),
    m_speed(0)
    {}
    Waypoint(WP_TYPE wptype,Vector3D pos,double speed,double heading):
        m_wptype(wptype),
        m_pos(pos),
        m_heading(heading),
        m_speed(speed)
    {}

    
    Waypoint& operator=(const Waypoint& x);
    bool operator ==(const Waypoint& b) const;
    bool operator !=(const Waypoint& b) const;

    Waypoint(const Waypoint& p);
    //void drawWaypoint(Vector3D offset);


    inline WP_TYPE wptype() const {return m_wptype;}
    inline void setWPType(WP_TYPE x){ m_wptype = x;}

    inline Vector3D  position() const {return m_pos;}
    inline void setPosition(Vector3D &x){ m_pos = x;}
    inline double heading() const {return m_heading;}
    inline void setHeading(double x){ m_heading = x;}
    inline double speed() const {return m_speed;}
    inline void setSpeed(double speed){ m_speed = speed;}
    bool isValid();
    //static Waypoint interpolateWaypoint(const Waypoint &from,const Waypoint &to,double &perc);
    
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW 
};

  ca_common::MissionWaypoint msgc(const Waypoint &wp);
  Waypoint  msgc(const ca_common::MissionWaypoint &wp);




}
#endif
