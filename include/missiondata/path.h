#ifndef PATH_H
#define PATH_H
//#include <QMutex>

#include <vector>

#include <ca_common/math.h>
#include <missiondata/waypoint.h>
#include <ca_common/Mission.h>

namespace CA
{

/**
* Set of waypoints makes up a path
*
*/
class Path
{
public:
  enum PATHTYPE{MT_BEST=ca_common::Mission::MT_BEST,MT_SHORTEST=ca_common::Mission::MT_SHORTEST,MT_LEASTFUEL=ca_common::Mission::MT_LEASTFUEL,MT_FASTEST=ca_common::Mission::MT_FASTEST};
  Path(){}
  Path(std::vector<Waypoint> path,std::string missionname,PATHTYPE pathtype):
    m_path(path),
    m_pathname(missionname),
    m_pathtype(pathtype)
  {}
  
  virtual ~Path()
  {}

  virtual Vector3D getMinVectorUpTo(const double &dist,const Vector3D &forv,double *distalong) ;  
  virtual void setPath(std::vector<Waypoint> path);
  virtual std::vector<Waypoint> getPath() const;
  virtual void setPathName(const std::string &pathname);
  virtual std::string getPathName() const;
  virtual void setPathType(const PATHTYPE &pt);
  virtual PATHTYPE getPathType() const;
  std::vector<Waypoint>  m_path;
protected:

  std::string m_pathname;
  PATHTYPE m_pathtype;
};

  
  ca_common::Mission msgc(const Path &path);
  Path  msgc(const ca_common::Mission &path);



}
#endif
